#!/usr/bin/python
# -*- coding: utf8 -*-
############################################################################
#    Copyright (C) 2006-2012 by Daniel 'Beorn' Mróz <beorn@cyberdeck.pl>   #
#                                                                          #
#    This program is free software; you can redistribute it and/or modify  #
#    it under the terms of the GNU Library General Public License as       #
#    published by the Free Software Foundation; either version 2 of the    #
#    License, or (at your option) any later version.                       #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU Library General Public     #
#    License along with this program; if not, write to the                 #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################

""" Pure Python module for running applications in daemon mode

    This module defines a Deamon class, that uses double-fork mechanism
    to enter daemon mode. It can be used for creating programs running
    in the background, with no controlling terminal.

    WARNING: Deamon mode is supported only by UNIX and UNIX-like operating
    systems.

    === Examples ===

    Using Daemon class for subclassing:

    >>> import daemon
    >>>
    >>> class MyDaemon(daemon.Daemon):
    ...     def __init__(self):
    ...         super(MyDaemon, self).__init__()
    ...
    >>>     def run(self):
    ...         # Code to execute in daemon mode
    ...
    >>>     def shutdown(self):
    ...         # Optional code to run before killing the process
    ...
    >>> d = MyDaemon()
    >>> d.set_pidfile("/var/run/myapp/myapp.pid", True)
    >>>
    >>> try:
    ...     if run_as_daemon:
    ...         d.daemonize()
    ...     else:
    ...         d.no_daemon()
    ... except daemon.DaemonError, exc:
    ...     # Something bad happened...
    >>>

    Using Daemon class directly:

    >>> import daemon
    >>>
    >>> def my_app():
    ...     # Code to execute in daemon mode
    ...
    >>> d = daemon.Daemon(my_app)
    >>> d.set_pidfile("/var/run/myapp/myapp.pid", True)
    >>>
    >>> try:
    ...     if run_as_daemon:
    ...         d.daemonize()
    ...     else:
    ...         d.no_daemon()
    ... except daemon.DaemonError, exc:
    ...     # Something bad happened...
    >>>
"""
import fcntl
import logging
import os
import sys
import threading


VERSION = (2, 0, 2)


class DaemonError(Exception):
    """ Exception class for Deamon errors """
    def __init__(self, msg):
        super(DaemonError, self).__init__(msg)



class ProcessLockingError(DaemonError):
    """ Locking exception class """
    def __init__(self, msg):
        super(ProcessLockingError, self).__init__(msg)



class IOHandler(object):
    """ Catch-class for standard IO rerouting

        This file-like object class is used to replace stdout
        and stderr streams and route messages to logging class.

        It should not be used outside the Deamon module.
    """
    def __init__(self, name):
        """ Create an IOHandler instance

            Argument `name' specifies the logger name used
            for that particular stream.
        """
        self.logger = logging.getLogger(name)


    def write(self, message):
        """ Write `message' into logger stream, stripping any
            trailing newlines
        """
        self.logger.info(message.strip('\n'))



class Daemon(object):
    """ Base class for Deamon objects

        This class can be subclassed or used directly to execute
        function in deamon mode. In the latter case, function must
        be passed as `func' argument to the constructor. Otherwise
        run() method will be called, wich should be overwritten in
        a subclass.
    """
    def __init__(self, func=None):
        self._func = func or self.run

        self.pid = None
        self.pidfile = None
        self._lock_me_up = False

        self._pfd = None
        self._shutting_down = threading.Lock()


    def set_pidfile(self, filename, lock=False):
        """ Set path to the file for PID storage

            If this method is called with a valid path in `filename'
            argument, process ID number will be stored in a given
            file. By default, no PID file is created. The file pointed
            to by `filename' does not have to exist, but bottom level
            directory must be writable by a user running the application.
            PID file will be removed on application close!

            If `lock' is True, PID file will be locked to prevent
            more than one instance of the same application from
            running.
        """
        self.pidfile = os.path.abspath(filename)
        self._lock_me_up = bool(lock)


    def daemonize(self):
        """ Switch to deamon mode and run the program

            From this point on I/O streams will be disconnected from
            the control terminal. No input will be available for data
            and all output will be routed to logging class.

            Warning: only standard descriptors will be closed.
        """
        sys.stdin.close()
        sys.stdout.close()
        sys.stderr.close()

        sys.stdout = IOHandler('stdout')
        sys.stderr = IOHandler('stderr')

        # First fork()
        if os.fork():
            sys.exit(0)

        os.chdir('/')
        os.setsid()
        os.umask(0)

        # Second fork()
        if os.fork():
            sys.exit(0)

        self._store_pid()

        try:
            self._func(True)
        except Exception, exc:
            sys.stderr.write('Critical error: {0}'.format(': '.join(map(str, exc.args))))

        self._unstore_pid()


    def kill(self, signal=15):
        """ Try to kill the running process

            By default signal 15 (SIGTERM) is sent to the running
            process. It can be overrriden by `signal' argument.
            Full list of signals and their numbers can be found in
            signal(7).

            Before killing the process, shutdown() is called once.

            This method should NOT be overwritten by a subclass.
        """
        if self._shutting_down.acquire(False):
            self.shutdown()
        os.kill(self.pid, signal)


    def no_daemon(self):
        """ Execute code without switching into daemon mode

            This method works the same as daemonize(), except it
            won't close and reroute standard descriptors and will
            not enter the daemon mode.

            You should NOT overwrite this method in a subclass.
        """
        self._store_pid()

        try:
            self._func(False)
        except Exception, exc:
            raise DaemonError('Critical error: {0}'.format(': '.join(map(str, exc.args))))

        self._unstore_pid()


    def shutdown(self):
        """ Method called before killing the process

            This method is called exactly once before attempting
            to kill the process using kill() method. All subsequent
            calls to kill() will not execute this method.

            By default shutdown() does nothing. It should be overwritten
            in a subclass.
        """
        pass


    def death_by_signal(self, signum, stack):
        """ Convienience method for connecting signals

            This method can be used as signal.signal() handler. It calls
            kill() method, setting the signal number to the value passed
            in signum.

            You should NOT overwrite this method in a subclass.

            Typical usage:

            >>> import daemon
            >>> import signal
            >>>
            >>> class MyDaemon(daemon.Daemon):
            ...     # ...
            ...
            >>> d = MyDaemon()
            >>>
            >>> for sig in (signal.SIGTERM, signal.SIGINT, signal.SIGQUIT):
            >>>     signal.signal(sig, d.death_by_signal)
        """
        self.kill(signum)


    def run(self, daemon_mode):
        """ Code to run in daemon mode

            When Daemon class is used as a base class, this method
            must be overwritten with a code to run in daemon mode.
            Do NOT call it directly. If going into daemon mode is
            undesireable, call no_daemon() method instead.

            `daemon_mode' argument will be set to True if daemonize()
            method was called or False if no_daemon() was executed.
        """
        raise NotImplementedError('This method must be implemented in a subclass')


    def _lock(self):
        if not self._lock_me_up:
            return

        try:
            fcntl.lockf(self._pfd, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            self._unstore_pid()
            raise ProcessLockingError('Another instance seems to be already running')


    def _unlock(self):
        if not self._lock_me_up:
            return

        try:
            fcntl.lockf(self._pfd, fcntl.LOCK_UN)
        except:
            pass


    def _store_pid(self):
        self.pid = os.getpid()
        if self.pidfile is None:
            return

        try:
            self._pfd = open(self.pidfile, 'a')
        except Exception, exc:
            raise ProcessLockingError('Cannot open PID file "{0}": {1}'.format(self.pidfile, ': '.join(map(str, exc.args))))

        self._lock()
        self._pfd.seek(0)
        self._pfd.truncate()
        self._pfd.write(str(self.pid))
        self._pfd.flush()


    def _unstore_pid(self):
        if self.pidfile is None:
            return

        self._unlock()

        try:
            self._pfd.close()
            os.unlink(self.pidfile)
        except:
            pass

        self.pidfile = None
